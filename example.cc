#include <iostream>
#include <tuple>
#include <typeinfo>

#include "titerator.hh"

#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>

using namespace std;
using namespace tuple_iteration;

#if ( __cplusplus > 201103L )
//C++14 (or C++1y), generic lambdas
auto func = [](auto t) { cout << t << '\t'; };

#elif ( __cplusplus == 201103L )
//C++11, use functor
struct _func
{
  template<typename T>
    inline void operator() (const T & t) const
    {
      cout << t << '\t';
    }
} func;
#endif
struct _func_i
{
  template<size_t i, typename T>
    inline void iterator(const T & t) const
    {
      cout << i << ":" << get<i>(t) << '\t';
    }
} func_i;

int main( int argc, char * argv[] )
{
  tuple<int, const char, double> t1 = make_tuple(1, 'A', 1.1);

  cout << "boost::fusion::foreach" << endl;
  boost::fusion::for_each(t1, func);
  cout << endl;
  cout << "tuple_iterator::tuple_foreach" << endl;
  for_each(t1, func);
  cout << endl;
  cout << "tuple_iteration::tuple_iterator::iterator" << endl;
  tuple_iterator<0,2>::iterator(t1, func);
  cout << endl;
  cout << "tuple_iteration::tuple_iterator::iterator_i" << endl;
  tuple_iterator<0,2>::iterator_i(t1, func_i);
  cout << endl;
  cout << "tuple_iteration::tuple_iterator::riterator" << endl;
  tuple_iterator<0,2>::riterator(t1, func);
  cout << endl;
  cout << "tuple_iteration::tuple_iterator::riterator_i" << endl;
  tuple_iterator<0,2>::riterator_i(t1, func_i);
  cout << endl;

  return 0;
}
