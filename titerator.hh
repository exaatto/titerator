#ifndef _TITERATOR_H
#define _TITERATOR_H
#include <tuple>

#define __TITER_RANGE_CHECK \
  do { \
    static_assert(( Begin >= 0), "Iteration begins before zero."); \
    static_assert(( End <= sizeof...(Tp)), "Iteration ends beyond tuple size."); \
  } while (0)

#define __TITER_ITERATOR_COND Begin < End

#define __TITER_ITERATOR_INTERFACE( NAME, COND, IS_CONST) \
  template<std::size_t Begin = _Begin, std::size_t End = _End, typename FuncT, typename... Tp> \
    static inline typename std::enable_if< COND , void>::type \
    NAME(IS_CONST std::tuple<Tp...> & t, const FuncT & f)

#define __TITER_ITERATOR_FUNCTION( NAME, ACTION, IS_CONST) \
  __TITER_ITERATOR_INTERFACE(NAME, (!(__TITER_ITERATOR_COND)), IS_CONST) \
    {\
      __TITER_RANGE_CHECK; \
    }\
  __TITER_ITERATOR_INTERFACE(NAME, (__TITER_ITERATOR_COND), IS_CONST) \
    {\
      __TITER_RANGE_CHECK; \
      ACTION; \
    }

#define __TITER_R_ITERATOR_ACTION \
      do {\
        tuple_iterator<Begin + 1, End>::iterator(t, f);\
        f(std::get<Begin>(t));\
      } while (0)
#define __TITER_R_ITERATOR_I_ACTION \
        do {\
        tuple_iterator<Begin + 1, End>::iterator_i(t, f);\
        f.iterator<Begin, __TITER_IS_CONST std::tuple<Tp...>>(t);\
      } while (0)

#define __TITER_ITERATOR_ACTION \
      do {\
        f(std::get<Begin>(t));\
        tuple_iterator<Begin + 1, End>::iterator(t, f);\
      } while (0)
#define __TITER_ITERATOR_I_ACTION \
        do {\
        f.iterator<Begin, __TITER_IS_CONST std::tuple<Tp...>>(t);\
        tuple_iterator<Begin + 1, End>::iterator_i(t, f);\
      } while (0)

namespace tuple_iteration {
  template <std::size_t _Begin, std::size_t _End>
    struct tuple_iterator
    {
#define __TITER_IS_CONST

      __TITER_ITERATOR_FUNCTION(iterator, __TITER_ITERATOR_ACTION, __TITER_IS_CONST)
      __TITER_ITERATOR_FUNCTION(iterator_i, __TITER_ITERATOR_I_ACTION, __TITER_IS_CONST)
      __TITER_ITERATOR_FUNCTION(riterator, __TITER_R_ITERATOR_ACTION, __TITER_IS_CONST)
      __TITER_ITERATOR_FUNCTION(riterator_i, __TITER_R_ITERATOR_I_ACTION, __TITER_IS_CONST)

#undef __TITER_IS_CONST
#define __TITER_IS_CONST const
      __TITER_ITERATOR_FUNCTION(iterator, __TITER_ITERATOR_ACTION, __TITER_IS_CONST)
      __TITER_ITERATOR_FUNCTION(iterator_i, __TITER_ITERATOR_I_ACTION, __TITER_IS_CONST)
      __TITER_ITERATOR_FUNCTION(riterator, __TITER_R_ITERATOR_ACTION, __TITER_IS_CONST)
      __TITER_ITERATOR_FUNCTION(riterator_i, __TITER_R_ITERATOR_I_ACTION, __TITER_IS_CONST)

    };

  template<typename FuncT, typename... Tp> 
    void for_each(std::tuple<Tp...>& t, const FuncT & f)
    {
      tuple_iterator<0, sizeof...(Tp)>::iterator(t, f);
    }
  template<typename FuncT, typename... Tp> 
    void for_each(const std::tuple<Tp...>& t, const FuncT & f)
    {
      tuple_iterator<0, sizeof...(Tp)>::iterator(t, f);
    }
}
#undef __TITER_IS_CONST
#undef __TITER_RANGE_CHECK
#undef __TITER_ITERATOR_INTERFACE
#endif
